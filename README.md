# Kustomized Helm

```bash
# This renders the Helm chart resources into a single Kubernetes manifest.
helm template \
  -f nlx-manager-values.yaml \
  manager \
  commonground/fsc-nlx-manager \
  > helm-chart-templated.yaml

# This will inject the proxy sidecar in the deployment without modifying the generated manifest.
kubectl kustomize .
```
